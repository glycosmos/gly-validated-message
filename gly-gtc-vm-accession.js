import {LitElement, html} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';

class GlyGtcVmAccession extends LitElement {
  static get properties() {
    return {
      accession: String,
      sampleids: Array
    };
  }

  render() {
    return html `
    <div>${this._processHtml()}</div>
   `;
  }

  // </div>
  // <div>
  // sampleids: ${this.sampleids}
  // accession: ${this.accession}
  // accessionhtml: <p>Accession Number:${this._accessionHtml()}</p>
  // ${this._massHtml()}
  // ${this._contributionHtml()}

  constructor() {
    super();
    console.log("constructor");
    this.accession="G00030MO";
    this.contributionTime=null;
    this.sampleids=[];
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
const host =  getdomain(location.href);
    const url1 = 'https://'+host+'/sparqlist/api/gtc_validated_message_acc?accession=' + this.accession;
    this.getSummary(url1);
  }

  getSummary(url1) {
    console.log(url1);
    var urls = [];

    urls.push(url1);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("summary");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    console.log("promises");
    console.log(promises);

    Promise.all(promises).then(results => {
      this.sampleids = results.pop();
      console.log("sampleids");
      console.log(sampleids);
    });
  }

  _processHtml(){
    return html`
      <table border="1">
        <thead>
          <tr>
            <th>graph</th>
            <th>validator/version/type/number</th>
            <th>validated message</th>
          </tr>
        </thead>
        <tbody>
        ${this.sampleids.map(item =>
          html`
          <tr>
            <td>${item.graph}</td>
            <td>${item.transform}</td>
            <td>${item.message}</td>
          </tr>
          `)}
        </tbody>
      </table>
   `;

    // return html`
    //   ${this.sampleids.map(i => html `<div>${i}</div>` )}
    // `;


  }
//   _processHtml() {
//     if (this.sampleids.length > 0) {
//       var popped = this.sampleids.pop();
//       // <p>Accession number:  ${item.AccessionNumber}</p>
//       // <p>Calculated Monoisotopic Mass: ${item.Mass}</p>
//       // <p>Contribution time: ${item.ContributionTime}</p>
//       return html`<p>Accession number: ${popped.AccessionNumber}</p><p>Calculated Monoisotopic Mass: ${popped.Mass}</p><p>Contribution time: ${popped.ContributionTime}</p>`;

//     } else {
//       return html`Could not retrieve`;
//     }
//   }
}

customElements.define('gly-gtc-vm-accession', GlyGtcVmAccession);
