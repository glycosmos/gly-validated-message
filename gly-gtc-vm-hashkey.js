import { LitElement, html, css } from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';

class GlyGtcVmHashkey extends LitElement {
  static get properties() {
    return {
      validatedReport: String,
      hashKey: String
    };
  }

  static get styles() {
    return css `
      li {
        list-style-type: none;
      }
      .results {
        color: red;
      }
      details {
        margin: 10px 0 0;
        font: 16px "Open Sans", "Arial", sans-serif;
      }
    `;
  }
  render() {
    return html `
      <div>${this._processHtml()}</div>
   `;
  }


  constructor() {
    super();
    console.log("constructor");
    this.validatedReport="";
    this.hashKey="";
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    const host =  getdomain(location.href);
    const url = 'https://'+host+'/sparqlist/api/gtc_validated_message?hash=' + this.hashKey;
    this.getValidatedMessage(url);
    console.log(this.hashKey);
  }


  getValidatedMessage(url) {
    console.log(url);
    var urls = [];

    urls.push(url);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("hash-key");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);

      this.validatedReport = results.pop();
    });
  }

  _processHtml() {
    if (this.validatedReport.length > 0) {
      console.log("this.validatedReport");
      console.log(this.validatedReport);
      // ex) wurcsvalid/WURCSFramework/0.1.0-SNAPSHOT
      if(/SNAPSHOT$/.test(this.validatedReport[0].transform)){
        // validation pass
        return html`
          <div class="passed">
            <p>Validation passed</p>
          </div>
        `;
      } else {
        // Warning or Error
        return html`
          <details>
            <summary>validation result</summary>
            ${this.validatedReport.map(item => html `
              <ul>
                <li>${item.graph}</li>
                <li>${item.transform}</li>
                <li class="results">${item.validated_result}</li>
              </ul>
            `)}
          </details>
        `; // end html
      }
    } else {
      return html`
        <p>Not processed</p>
      `;
    }
  }

}

customElements.define('gly-gtc-vm-hashkey', GlyGtcVmHashkey);
